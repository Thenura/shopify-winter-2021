import React from "react";
import {
  Layout,
  Page,
  Card,
  ResourceList,
  Modal,
  TextContainer,
  Heading,
  Banner,
} from "@shopify/polaris";
import {IDictionary, IMovieItem, IFullMovieItem} from "./utils/interfaces";
import {CardItem, SearchField, MovieThumbnail, MovieModal} from "./components";
import {getMovieByID, getMoviesByQuery} from "./networking/api";

interface IAppProps {}

interface IAppState {
  searchQuery: string;
  queryResults: IMovieItem[];
  nominations: IDictionary<IMovieItem>;
  isModalOpen: boolean;
  currentMovie: any;
  errorMessage: string;
}

export class App extends React.Component<IAppProps, IAppState> {
  constructor(props: IAppProps) {
    super(props);
    this.state = {
      currentMovie: null,
      searchQuery: "",
      nominations: {},
      isModalOpen: false,
      queryResults: [],
      errorMessage: "",
    };
  }

  componentDidMount = () => {
    const result: any = localStorage.getItem("nominations");

    if (result !== null) {
      const nominations = JSON.parse(result);
      this.setState({
        nominations,
      });
    }
  };

  updateText = async (value: string) => {
    this.setState({
      searchQuery: value,
    });

    // Reset the search results when the value is empty
    if (value === "" && this.state.queryResults.length !== 0) {
      this.setState({
        queryResults: [],
        errorMessage: "",
      });
    }

    try {
      const data = await getMoviesByQuery(value);

      if (data.Response === "True") {
        const {Search} = data;
        this.setState({
          queryResults: Search,
          errorMessage: "",
        });
      } else {
        if (data.Error === "Movie not found!") {
          this.setState({
            errorMessage: "Movie not found!",
          });
        } else if (data.Error === "Too many results.") {
          this.setState({
            errorMessage: "Too many results.",
          });
        }
      }
    } catch (e) {
      console.log(e);
    }
  };

  onPressMovieNomination = (movie: IMovieItem) => {
    // Nominations Data Structure - Dictionary with IMDB ID as Keys
    /*
      nominations: {
        123: {Title: 'Spider Man', ...},
        351: {Title: 'Pokemon', ...},
      }
    */

    // If the movie has already been selected, remove it from the state
    if (this.state.nominations[movie.imdbID]) {
      // Removes movie given its respectivie id
      const {
        [movie.imdbID]: removedNomination,
        ...newNominationState
      } = this.state.nominations;
      this.setState({
        nominations: newNominationState,
      });
    } else {
      // If the movie has not been selected, add it to the state
      // and only add if there are less than 5 nominations
      if (Object.keys(this.state.nominations).length < 5) {
        // Save to local storage when 5 nominations have been reached
        if (Object.keys(this.state.nominations).length + 1 === 5) {
          // Due to asychronous behaviour, add to temporary variable and set to local storage
          const temp = {
            ...this.state.nominations,
            [movie.imdbID]: movie,
          };
          localStorage.setItem("nominations", JSON.stringify(temp));
        }
        this.setState({
          nominations: {
            ...this.state.nominations,
            [movie.imdbID]: movie,
          },
        });
      }
    }
  };

  renderAlert = () => {};
  // Retrieves the movie object by a given ID
  onPressMovieResource = async (item: IMovieItem) => {
    try {
      const movie: IFullMovieItem = await getMovieByID(item.imdbID);
      this.setState({
        isModalOpen: true,
        currentMovie: movie,
      });
    } catch (e) {
      console.log(e);
    }
  };

  // Triggered when the nomination button is pressed on the modal
  // Retrieving the respective object from state given key
  onPressModalAdd = () => {
    for (let i = 0; i < this.state.queryResults.length; i++) {
      const item: IMovieItem = this.state.queryResults[i];
      if (item.imdbID === this.state.currentMovie!.imdbID) {
        this.onPressMovieNomination(item);
        break;
      }
    }
    this.setState({isModalOpen: false});
  };

  renderModalSection = () => {
    // Add the fields you want from the API -> Will let you render it dynamically
    const fields = ["Plot", "Genre", "Actors"];

    return fields.map((field: string) => {
      return (
        <div>
          {this.state.currentMovie[field] !== "N/A" ? (
            <Modal.Section>
              <Heading>{field}</Heading>
              <TextContainer>
                <p>{this.state.currentMovie[field]}</p>
              </TextContainer>
            </Modal.Section>
          ) : null}
        </div>
      );
    });
  };

  isNominationButtonDisabled = (item: IMovieItem) => {
    if (this.state.nominations[item.imdbID]) return true;
    if (Object.keys(this.state.nominations).length === 5) return true;

    return false;
  };

  render() {
    return (
      <Page title="The Shoppies">
        {Object.keys(this.state.nominations).length === 5 && (
          <Banner
            title="You have successfully created 5 nominations."
            status="success"
          />
        )}
        <Layout>
          <Layout.Section>
            <Card title="Movie Titles" sectioned>
              <div style={{height: "70px"}}>
                <SearchField
                  updateText={this.updateText}
                  searchQuery={this.state.searchQuery}
                />
              </div>
              {this.state.errorMessage && (
                <div>
                  <Heading>{this.state.errorMessage}</Heading>
                </div>
              )}
            </Card>
          </Layout.Section>
        </Layout>
        <Layout>
          <Layout.Section oneHalf>
            <Card title={`Results for "${this.state.searchQuery}"`} sectioned>
              {this.state.searchQuery === "" &&
                this.state.queryResults.length === 0 && (
                  <p>Search a movie or a television show.</p>
                )}
              {this.state.queryResults.length !== 0 && (
                <ResourceList
                  resourceName={{singular: "movie", plural: "movies"}}
                  items={this.state.queryResults}
                  // showHeader={true}
                  renderItem={(item: IMovieItem) => {
                    const media = <MovieThumbnail item={item} />;

                    return (
                      <CardItem
                        item={item}
                        buttonText={
                          this.state.nominations[item.imdbID]
                            ? "Nominated"
                            : "Info"
                        }
                        buttonDisabled={this.isNominationButtonDisabled(item)}
                        onPressResourceItem={() => {
                          // Only open the modal if the item has not been added
                          if (
                            this.state.nominations[item.imdbID] === undefined &&
                            !this.isNominationButtonDisabled(item)
                          ) {
                            this.onPressMovieResource(item);
                          }
                        }}
                        media={media}
                      />
                    );
                  }}
                />
              )}
            </Card>
          </Layout.Section>
          <Layout.Section oneHalf>
            <Card title="Nominations" sectioned>
              {this.state.searchQuery === "" &&
                Object.keys(this.state.nominations).length === 0 && (
                  <p>Your movie and show nominations will appear here.</p>
                )}
              {Object.keys(this.state.nominations).length !== 0 && (
                <ResourceList
                  resourceName={{singular: "movie", plural: "movies"}}
                  items={Object.keys(this.state.nominations)}
                  // showHeader={true}
                  renderItem={(movieID: string) => {
                    const item = this.state.nominations[movieID];
                    const media = <MovieThumbnail item={item} />;

                    return (
                      <CardItem
                        item={item}
                        buttonText="Remove"
                        onPressResourceItem={() =>
                          this.onPressMovieNomination(item)
                        }
                        media={media}
                      />
                    );
                  }}
                />
              )}
            </Card>
          </Layout.Section>
        </Layout>
        {this.state.currentMovie && (
          <MovieModal
            isModalOpen={this.state.isModalOpen}
            onClose={() => {
              this.setState({isModalOpen: !this.state.isModalOpen});
            }}
            movie={this.state.currentMovie}
            onPrimaryAction={() => {
              this.onPressModalAdd();
            }}
            primaryButtonText="Nominate"
            onSecondaryAction={() => {
              this.setState({isModalOpen: !this.state.isModalOpen});
            }}
            secondaryButtonText="Cancel"
          >
            {this.renderModalSection()}
          </MovieModal>
        )}
      </Page>
    );
  }
}
