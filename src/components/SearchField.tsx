import React from "react";
import {Autocomplete, Icon} from "@shopify/polaris";
import {SearchMinor} from "@shopify/polaris-icons";

interface ISearchFieldProps {
  searchQuery: string;
  updateText: (text: string) => void;
  label?: string;
  placeholder?: string;
}

const SearchField = (props: ISearchFieldProps) => {
  return (
    <Autocomplete.TextField
      onChange={props.updateText}
      label={props.label ? props.label : ""}
      value={props.searchQuery}
      prefix={<Icon source={SearchMinor} color="inkLighter" />}
      placeholder={props.placeholder ? props.placeholder : "Search"}
    />
  );
};

export default SearchField;
