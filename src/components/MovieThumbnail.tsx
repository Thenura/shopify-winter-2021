import React from "react";
import {Thumbnail} from "@shopify/polaris";
import {IMovieItem} from "../utils/interfaces";

const noImageURL =
  "https://11luuvtufne6f2y33i1nvedi-wpengine.netdna-ssl.com/wp-content/uploads/2017/10/no-image-icon.png";

interface IMovieThumbnailProps {
  item: IMovieItem;
  alt?: string;
}

const MovieThumbnail = (props: IMovieThumbnailProps) => {
  const item: IMovieItem = props.item;
  return (
    <Thumbnail
      size="large"
      source={item.Poster !== "N/A" ? item.Poster : noImageURL}
      alt={props.alt ? props.alt : ""}
    />
  );
};

export default MovieThumbnail;
