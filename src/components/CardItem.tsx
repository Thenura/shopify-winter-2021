import React from "react";
import {ResourceItem, TextStyle, Button} from "@shopify/polaris";
import {IMovieItem} from "../utils/interfaces";

interface ICardItem {
  item: IMovieItem;
  buttonText: string;
  onPressButton?: () => void;
  onPressResourceItem: () => void;
  media: any; // Used for the thumbnail component
  buttonDisabled?: boolean; // Used to disable and enable the nomination button
}
const CardItem = (props: any) => {
  const item: IMovieItem = props.item;

  return (
    <ResourceItem
      accessibilityLabel={`View details for ${props.title}`}
      onClick={props.onPressResourceItem}
      id={item.Title}
      media={props.media}
    >
      <div className="resource-item-container">
        <div>
          <h3>
            <TextStyle variation="strong">{item.Title}</TextStyle>
          </h3>
          <h3>
            <TextStyle variation="subdued">{item.Year}</TextStyle>
          </h3>
          <h3>
            <TextStyle variation="subdued">
              {item.Type.charAt(0).toUpperCase() + item.Type.slice(1)}
            </TextStyle>
          </h3>
        </div>
        <div>
          <Button
            primary={props.buttonText === "Remove" ? true : false}
            onClick={props.onPressButton ? props.onPressButton : null}
            disabled={props.buttonDisabled ? props.buttonDisabled : false}
          >
            {props.buttonText}
          </Button>
        </div>
      </div>
    </ResourceItem>
  );
};

export default CardItem;
