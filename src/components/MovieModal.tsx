import React, {ReactNode} from "react";
import {Modal} from "@shopify/polaris";
import {IFullMovieItem} from "../utils/interfaces";

interface IMovieModalModal {
  children: ReactNode;
  isModalOpen: boolean;
  movie: IFullMovieItem;
  onClose: () => void;
  onPrimaryAction: () => void;
  onSecondaryAction: () => void;
  primaryButtonText: string;
  secondaryButtonText: string;
}

const MovieModal = (props: IMovieModalModal) => {
  const movie: IFullMovieItem = props.movie;
  return (
    <Modal
      open={props.isModalOpen}
      onClose={props.onClose}
      title={movie.Title}
      primaryAction={{
        content: props.primaryButtonText,
        onAction: props.onPrimaryAction,
      }}
      secondaryActions={[
        {
          content: props.secondaryButtonText,
          onAction: props.onSecondaryAction,
        },
      ]}
    >
      {props.children}
    </Modal>
  );
};

export default MovieModal;
