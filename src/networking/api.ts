import axios from "axios";

const BASEURL = "https://www.omdbapi.com/?apikey=22f0a360";

export const getMovieByID = async (_id: string) => {
  const {data} = await axios.get(`${BASEURL}&i=${_id}`);

  return data;
};

export const getMoviesByQuery = async (query: string) => {
  const {data} = await axios.get(`${BASEURL}&s=${query}`);

  return data;
};
