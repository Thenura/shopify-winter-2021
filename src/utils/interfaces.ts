export interface IDictionary<T> {
  [key: string]: T;
}

// Interface for Search Query Movie Item
export interface IMovieItem {
  Title: string;
  Year: string;
  Type: string;
  imdbID: string;
  Poster: string;
}

// Interface for Search By Movie ID
export interface IFullMovieItem {
  Title?: string;
  Year?: string;
  Rated?: string;
  Released?: string;
  Runtime?: string;
  Genre?: string;
  Director?: string;
  Writer?: string;
  Actors?: string;
  Plot?: string;
  Language?: string;
  Country?: string;
  Awards?: string;
  Poster?: string;
  Ratings?: any;
  Metascore?: string;
  imdbRating?: string;
  imdbVotes?: string;
  imdbID?: string;
  Type?: string;
  DVD?: string;
  BoxOffice?: string;
  Production?: string;
  Website?: string;
  Response?: string;
}
