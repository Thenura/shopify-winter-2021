# The Shoppies

A front-end application used to manage movie nominations for the upcoming Shoppies.

## Project Link
[Here is a link to the live application.](https://modest-pasteur-2522d5.netlify.app/)

## Installation

Use the package manager [yarn](https://yarnpkg.com/) to install Shoppies.

```bash
yarn install
```

Use the following script to run the project.

```bash
yarn start
```

## Bonus Points Baby!
### Movie Modal 
When the information button is pressed for each movie, a modal appears and displays the **plot, genre and actors** in the respective movie. The user is able to know more about the movie before giving a nomination!

![Movie Information Modal](images/pop-up.gif)

### Save Nominations 
When the user has successfully completed 5 movie/show nominations, it is added to the local storage. If the user proceeds to leave the application, when it is re-visited later, those choices still persist. 

![Persist Movie Nominations](images/save-history.gif)

### Shopify UI Elements
Shopify's Polaris design system was used to build this application. Each implementation used best practice suggestions and content guidelines during development.

![Shopify Polaris](images/polaris.gif)


## License
[MIT](https://choosealicense.com/licenses/mit/)